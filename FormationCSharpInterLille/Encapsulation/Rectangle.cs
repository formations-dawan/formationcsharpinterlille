﻿namespace FormationCSharpInterLille.Encapsulation
{
    /* Selon une spécification donnée, on doit pouvoir calculer l'aire du rectangle et le redimensionner.
     * Il n'y a à priori pas d'autres besoins.
     * Le but de l'encapsulation est d'extérioriser le moins possible la structure de donnée interne (l'état) de l'objet.
     * On va donc créer une méthode Aire() pour retourner l'aire calculée sur la base de la largeur et la longueur.
     * On va également créer une méthode Redim() pour redimensionner le rectangle, avec la nouvelle largeur et longueur.
     * Au final, nous n'avons pas besoin d'exposer les propriétés Largeur et Longueur en dehors de la classe (public)
     *  car elles ne seront pas utiles.
     */
    internal class Rectangle
    {
        #region Champs et proprietes
        /* On utilise des propriétés complètes (avec backing fields)
         * pour exécuter une logique particulière lors de la lecture/écriture
         */
        private int largeur;
        private int longueur;

        private int Largeur
        {
            get => largeur;
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException("La largeur ne peut pas être négative !");
                largeur = value; //cette instruction ne sera pas excécutée si l'exception est déclenchée
            }
        }
        private int Longueur
        {
            get => longueur;
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException("La longueur ne peut pas être négative !");
                longueur = value;
            }
        }
        #endregion

        #region Constructeurs
        public Rectangle(int largeur, int longueur)
        {
            //Mauvaise solution
            /*if (longueur < 0) throw new ArgumentOutOfRangeException("La longueur ne peut pas être négative !");
            if (largeur < 0) throw new ArgumentOutOfRangeException("La largeur ne peut pas être négative !");

            this.largeur = largeur;
            this.longueur = longueur;*/

            Redim(largeur, longueur);
        }
        #endregion

        #region Methodes
        // Préférer les propriétés dans le cas où un jour on effectue une transformation lors de la lecture d'un champ
        public int Aire() => Largeur * Longueur;

        public void Redim(int largeur, int longueur)
        {
            // Préférer les propriétés pour ne pas avoir besoin de répéter les mêmes vérifications lors de l'écriture d'un champ
            Longueur = longueur;
            Largeur = largeur;
        }
        #endregion
    }
}
