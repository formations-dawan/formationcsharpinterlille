﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.TellDontAsk.Exemple2
{
    internal class PercelDeliveryService
    {
        public void DeliverParcel(long customerId)
        {
            // Il faut bien entendu que la liste des clients soit initialisée dans Supplier
            var supp = new Supplier();
            supp.Deliver(customerId);
        }
    }
}
