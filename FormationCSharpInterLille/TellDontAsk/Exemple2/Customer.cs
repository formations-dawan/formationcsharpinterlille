﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.TellDontAsk.Exemple2
{
    internal class Customer
    {
        public long Id { get; internal set; }

        public List<Percel> Percels { get; internal set; }

        public string CustomerAddress { get; internal set; }
    }
}
