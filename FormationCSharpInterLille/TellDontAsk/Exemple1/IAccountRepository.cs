﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.TellDontAsk.Exemple1
{
    internal interface IAccountRepository
    {
        Account FindById(int accountId);
        void Save(Account account);
    }
}
