﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.TellDontAsk.Exemple1
{
    internal class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        void IAccountService.Withdraw(int accountId, double amount)
        {
            var account = _accountRepository.FindById(accountId);
            /* Je dis à Account quoi faire (effectuer un retrait) ; c'est sa responsabilité.
             * Je ne demande pas à Account son état pour prendre une décision et le modifier ensuite.
             */
            account.Withdraw(amount);
            _accountRepository.Save(account);
        }
    }
}
