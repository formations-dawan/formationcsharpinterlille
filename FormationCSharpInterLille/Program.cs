﻿// See https://aka.ms/new-console-template for more information
using FormationCSharpInterLille.Aggregation.Faible;
using FormationCSharpInterLille.Aggregation.Forte;
using FormationCSharpInterLille.DesignPatterns.Comportement.ChainOfResponsibility;
using FormationCSharpInterLille.DesignPatterns.Comportement.Observer;
using FormationCSharpInterLille.DesignPatterns.Structure.Adapter;
using FormationCSharpInterLille.DesignPatterns.Structure.Decorator;
using FormationCSharpInterLille.DesignPatterns.Structure.Proxy;
using FormationCSharpInterLille.Encapsulation;
using FormationCSharpInterLille.Genericite;

Console.WriteLine("Hello, World!");

#region Genericite
/* On utilise l'interface pour l'objet pDao, avec comme implémentation ProduitDao.
 * On pourra ainsi utiliser les méthodes qui sont implémentées explicitement dans ProduitDao,
 *  car elles ne sont pas disponibles si on utilise directement la classe concrète.
 */
IDao<Produit, int> pDao = new ProduitDao();

GenClass<int> genClass = new()
{
    A = 12, // A est typé comme int à l'utilisation
    B = 13  // B est typé comme int à l'utilisation
};

genClass.Echanger();

Console.WriteLine(genClass.A);

var jsonRes = JsonUtils.EnJson(new Produit { Id = 1, Description = "RTX 4090", Prix = 2000 });
Console.WriteLine(jsonRes);
#endregion

#region Encapsulation
/* On ne connait pas réellement la structure interne de Rectangle.
 * Les seules opérations possibles sont le calcul de l'aire et le redimensionnement.
 */
var r = new Rectangle(12, 50);
var aire = r.Aire();
r.Redim(14, 100);

try
{
    var r1 = new Rectangle(-5, 10); // ArgumentOutOfRangeException déclenchée par la propriété Largeur lors de l'affectation
}
catch (Exception)
{
    Console.WriteLine("Rectangle incorrect");
}

Console.WriteLine(aire);
#endregion

#region Aggregation
//Forte
var v = new Voiture(new Moteur("diesel"));
v.Garer(new Parking()); //Association

var v2 = new Voiture("hydrogene");

//Faible
Client c = new();
c.Nom = "NomClient";
c.Prenom = "PrenomClient";
c.Adresse = new()
{
    Num = 15,
    Rue = "Arsoval",
    Ville = "Paris",
    CP = 75015
};
#endregion

#region Design Patterns - Adapter
/*IJsonAdapter adapter = new JsonAdapter(new ContactRepository());
string json = adapter.RecupererContactsJson("contacts.xml");

Console.WriteLine(json);*/
#endregion

#region Design Patterns - Decorator
IVoiture voiture = new VoitureBasique();
voiture.Assembler();
Console.WriteLine("-----------");

voiture = new SportDecorator(voiture);
voiture.Assembler();
Console.WriteLine("-----------");

voiture = new LuxeDecorator(voiture);
voiture.Assembler();
Console.WriteLine("-----------");

IVoiture voitureSportLuxe = new SportDecorator(new LuxeDecorator(new VoitureBasique()));
voitureSportLuxe.Assembler();
#endregion

#region Design Patterns - Proxy
//on passe par le proxy pour réaliser l'opération RecupererContenu();
IMessage message = new ProxyMessage(new MessageUtilisateur("bonjour"));
Console.WriteLine(message.RecupererContenu());
#endregion

#region Design Patterns - Chain Of Responsibility
//la chaine d'objet qui va gérer le traitement d'une plainte
MembreEquipe formateur = new Formateur("Florian", new DirPeda("Jean-François", new Directeur("Jérôme", null)));

Console.WriteLine("---REQ1---");
formateur.Handle(new Plainte(123, 1, "req1", Plainte.EtatPlainte.Ouvert));

Console.WriteLine("---REQ2---");
formateur.Handle(new Plainte(124, 2, "req2", Plainte.EtatPlainte.Ouvert));

Console.WriteLine("---REQ3---");
formateur.Handle(new Plainte(123, 3, "req3", Plainte.EtatPlainte.Ouvert));

#endregion

#region Design Patterns - Observer
var article = new Article() { Description = "RTX 4090", Prix = 2000 };

IDisposable disC1 = article.Subscribe(new FormationCSharpInterLille.DesignPatterns.Comportement.Observer.Contact { Nom = "Florian" });
IDisposable disC2 = article.Subscribe(new FormationCSharpInterLille.DesignPatterns.Comportement.Observer.Contact { Nom = "Grégory" });

article.Prix = 1500; //le changement déclenche les notifications

disC1.Dispose(); //désabonnement de C1

article.Prix = 1000;
#endregion