﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Proxy
{
    // Joue le rôle d'intermédiaire pour contrôler / relayer les commandes
    internal class ProxyMessage : IMessage
    {
        private IMessage _messageProxifie;

        public ProxyMessage(IMessage messageProxifie)
        {
            _messageProxifie = messageProxifie;
        }

        string IMessage.RecupererContenu()
        {
            var contenuOriginal = _messageProxifie.RecupererContenu();
            // faire ici des vérifications ou des transformations
            var contenuTransforme = contenuOriginal.ToUpper();
            return contenuTransforme;
        }
    }
}
