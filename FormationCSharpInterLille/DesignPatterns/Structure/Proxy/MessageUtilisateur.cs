﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Proxy
{
    internal class MessageUtilisateur : IMessage
    {
        private string _contenu;

        public MessageUtilisateur(string contenu)
        {
            _contenu = contenu;
        }

        string IMessage.RecupererContenu()
        {
            return _contenu;
        }
    }
}
