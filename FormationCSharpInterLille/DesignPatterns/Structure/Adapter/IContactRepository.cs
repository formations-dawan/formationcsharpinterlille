﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Adapter
{
    internal interface IContactRepository
    {
        string RecupererContactsXml(string cheminFichier);

        List<Contact> DepuisXml(string xml);
    }
}
