﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Adapter
{
    internal class Contact
    {
        public int Id { get; set; }

        public string Nom { get; set; }
    }
}
