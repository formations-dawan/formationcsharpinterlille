﻿using FormationCSharpInterLille.Genericite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Adapter
{
    internal class JsonAdapter : IJsonAdapter
    {
        private IContactRepository contactRepository;

        public JsonAdapter(IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        string IJsonAdapter.RecupererContactsJson(string cheminFichier)
        {
            string xml = contactRepository.RecupererContactsXml(cheminFichier);
            // Transformer en JSON
            List<Contact> lstC = contactRepository.DepuisXml(xml);
            return JsonUtils.EnJson(lstC);
        }
    }
}
