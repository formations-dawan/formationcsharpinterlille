﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Adapter
{
    internal class ContactRepository : IContactRepository
    {
        List<Contact> IContactRepository.DepuisXml(string xml)
        {
            XmlDocument doc = new();
            doc.LoadXml(xml);

            XmlNodeList noeuds = doc.DocumentElement.SelectNodes("/Contacts/Contact");

            List<Contact> lstRes = new();
            foreach (XmlNode noeud in noeuds)
            {
                var c = new Contact
                {
                    Id = Convert.ToInt32(noeud.Attributes["id"].Value),
                    Nom = noeud.ChildNodes[0].InnerText
                };

                lstRes.Add(c);
            }

            return lstRes;
        }

        string IContactRepository.RecupererContactsXml(string cheminFichier)
        {
            using var sr = new StreamReader(cheminFichier);

            var res = sr.ReadToEnd();

            return res;
        }
    }
}
