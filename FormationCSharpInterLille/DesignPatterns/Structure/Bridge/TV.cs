﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Bridge
{
    internal class TV : IAppareil
    {
        public void Allumer()
        {
            throw new NotImplementedException();
        }

        public void ChangerChaine(int ch)
        {
            throw new NotImplementedException();
        }

        public void ChangerVolume(int v)
        {
            throw new NotImplementedException();
        }

        public bool EstAllume()
        {
            throw new NotImplementedException();
        }

        public void Eteindre()
        {
            throw new NotImplementedException();
        }
    }
}
