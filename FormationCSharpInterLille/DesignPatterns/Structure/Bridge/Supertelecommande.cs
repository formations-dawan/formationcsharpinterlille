﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Bridge
{
    internal class Supertelecommande : TelecommandeBasique
    {
        public void Sourdine() => ChangerVolume(0);
    }
}
