﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Bridge
{
    internal class TelecommandeBasique : Telecommande
    {
        public override void AllumerOuEteindre()
        {
            if (appareil.EstAllume())
                appareil.Eteindre();
            else
                appareil.Allumer();
        }

        public override void ChangerChaine(int c)
        {
            appareil.ChangerChaine(c);
        }

        public override void ChangerVolume(int v)
        {
            appareil.ChangerVolume(v);
        }
    }
}
