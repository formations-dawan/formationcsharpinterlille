﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Bridge
{
    internal abstract class Telecommande
    {
        protected IAppareil appareil;

        public void DefinirAppareil(IAppareil appareil)
        {
            this.appareil = appareil;
        }

        public abstract void ChangerVolume(int v);
        public abstract void ChangerChaine(int c);
        public abstract void AllumerOuEteindre();
    }
}
