﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Decorator
{
    internal class LuxeDecorator : VoitureDecorator
    {
        public LuxeDecorator(IVoiture voiture) : base(voiture)
        {
        }

        public override void Assembler()
        {
            base.Assembler();
            Console.WriteLine("--avec options Luxe.");
            //base.Assembler(); -- ne pas le faire après avoir ajouté un comportement supplémentaire ; la base doit être déjà créée
        }
    }
}
