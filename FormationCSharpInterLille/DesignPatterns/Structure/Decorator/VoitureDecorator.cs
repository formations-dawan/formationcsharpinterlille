﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Decorator
{
    internal abstract class VoitureDecorator : IVoiture
    {
        protected IVoiture voiture;

        protected VoitureDecorator(IVoiture voiture)
        {
            this.voiture = voiture;
        }

        public virtual void Assembler()
        {
            voiture.Assembler();
        }
    }
}
