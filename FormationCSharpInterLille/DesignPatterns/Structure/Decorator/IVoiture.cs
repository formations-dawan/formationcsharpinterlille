﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Structure.Decorator
{
    internal interface IVoiture
    {
        void Assembler();
    }
}
