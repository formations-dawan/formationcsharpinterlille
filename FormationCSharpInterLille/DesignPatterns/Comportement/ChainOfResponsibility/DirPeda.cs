﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class DirPeda : MembreEquipe
    {
        public DirPeda(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            if (requete.Type == 2)
            {
                Console.WriteLine("traitement par le directeur pédagogique");
                requete.Etat = Plainte.EtatPlainte.Ferme;
            }
            else if (successeur != null)
            {
                Console.WriteLine("le directeur pédagogique a remonté la demande à son successeur");
                successeur.Handle(requete);
            }
        }
    }
}
