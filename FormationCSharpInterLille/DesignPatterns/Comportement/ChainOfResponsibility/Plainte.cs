﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Plainte
    {
        public int NumApprenant { get; set; }
        public int Type { get; set; } //selon le type, la plainte sera traitée par un membre de l'équipe ; 1 : formateur, 2 : dir péda, 3 : directeur
        public string Message { get; set; }
        public EtatPlainte Etat { get; set; }

        public enum EtatPlainte
        {
            Ouvert,
            Ferme
        }

        public Plainte(int numApprenant, int type, string message, EtatPlainte etat)
        {
            NumApprenant = numApprenant;
            Type = type;
            Message = message;
            Etat = etat;
        }
    }
}