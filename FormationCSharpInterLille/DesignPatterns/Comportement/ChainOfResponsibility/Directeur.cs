﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Directeur : MembreEquipe
    {
        public Directeur(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            Console.WriteLine("traitement par le directeur");
            requete.Etat = Plainte.EtatPlainte.Ferme;
        }
    }
}
