﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal class Formateur : MembreEquipe
    {
        public Formateur(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void Handle(Plainte requete)
        {
            if (requete.Type == 1)
            {
                Console.WriteLine("traitement par le formateur");
                requete.Etat = Plainte.EtatPlainte.Ferme;
            }
            else if (successeur != null)
            {
                Console.WriteLine("le formateur a remonté la demande à son successeur");
                successeur.Handle(requete);
            }
        }
    }
}
