﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Comportement.ChainOfResponsibility
{
    internal abstract class MembreEquipe //Handler (dans le schéma UML)
    {
        protected string Nom;
        protected MembreEquipe successeur; //pour avoir le membre suivant dans la responsabilité

        protected MembreEquipe(string nom, MembreEquipe successeur)
        {
            Nom = nom;
            this.successeur = successeur;
        }

        public abstract void Handle(Plainte requete);
    }
}
