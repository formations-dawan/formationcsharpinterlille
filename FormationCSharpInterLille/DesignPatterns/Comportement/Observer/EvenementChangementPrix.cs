﻿namespace FormationCSharpInterLille.DesignPatterns.Comportement.Observer
{
    internal class EvenementChangementPrix
    {
        public DateTime Date { get; set; }
        public string NotifMessage { get; set; }

        public EvenementChangementPrix(DateTime date, string notifMessage)
        {
            Date = date;
            NotifMessage = notifMessage;
        }
    }
}