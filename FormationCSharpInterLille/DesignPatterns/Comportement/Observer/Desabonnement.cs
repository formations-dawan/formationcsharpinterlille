﻿namespace FormationCSharpInterLille.DesignPatterns.Comportement.Observer
{
    internal class Desabonnement : IDisposable
    {
        private List<IObserver<EvenementChangementPrix>> _observers; //cette liste est la même que dans article (partage d'adresse)
        private IObserver<EvenementChangementPrix> _observer;

        public Desabonnement(List<IObserver<EvenementChangementPrix>> observers, IObserver<EvenementChangementPrix> observer)
        {
            _observers = observers;
            _observer = observer;
        }

        public void Dispose()
        {
            if (_observers != null)
                _observers.Remove(_observer); //on supprime l'observateur de la liste ; article n'enverra plus de notifications
        }
    }
}