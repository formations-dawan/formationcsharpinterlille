﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.DesignPatterns.Comportement.Observer
{
    internal class Article : IObservable<EvenementChangementPrix>
    {
        public string Description { get; set; }

        private List<IObserver<EvenementChangementPrix>> _observers;

        public Article()
        {
            _observers = new List<IObserver<EvenementChangementPrix>>();
        }

        public IDisposable Subscribe(IObserver<EvenementChangementPrix> observer)
        {
            
            if (!_observers.Contains(observer))
                _observers.Add(observer);

            return new Desabonnement(_observers, observer); //on crée un nouvel objet autonome pour se désinscrire
        }

        private double _prix;

        public double Prix
        {
            get => _prix;
            set
            {
                _prix = value;
                //objet encapsulant les détails de l'évènement / notifications
                var e = new EvenementChangementPrix(DateTime.Now, "Changement de prix pour " + Description + " : " + Prix);

                foreach (var observateur in _observers)
                {
                    observateur.OnNext(e);
                }
            }
        }
    }
}
