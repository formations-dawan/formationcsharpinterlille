﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Polymorphisme
{
    internal class PolyTest
    {
        //ad-hoc (ou niveau de fonction avec des GetType)
        public static void Acheter(Object obj)
        {
            if (obj.GetType().Equals(typeof(Table)))
            {
                IPliable table = (Table)obj;
                table.Plier();
            }
        }

        //Sous-typage (héritage, implémentation)
        public static void Acheter(IPliable pliable)
        {
            pliable.Plier();
            // pas besoin de tester le type à chaque fois
        }

        //Types paramétriques (généricité)
        public static void Acheter<TPliable>(TPliable pliable) where TPliable : IPliable
        {
            pliable.Plier();
            // pas besoin de tester le type à chaque fois
        }
    }
}
