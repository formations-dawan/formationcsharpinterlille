﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Polymorphisme
{
    internal class Table : IPliable
    {
        string IPliable.Deplier() => "Deplier...";

        string IPliable.Plier() => "Plier...";
    }
}
