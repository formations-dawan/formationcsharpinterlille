﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Genericite
{
    
    [DataContract] //Utilisé pour la sérialisation
    /* Produit hérite de TableBase avec comme argument générique le type int.
     * La propriété Id héritée sera donc de type int.
     */
    internal class Produit : TableBase<int>
    {
        [DataMember] //Utilisé pour la sérialisation
        public string Description { get; set; }

        [DataMember]
        public double Prix { get; set; }
    }
}
