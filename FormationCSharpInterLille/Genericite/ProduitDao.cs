﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Genericite
{
    /* Implémente l'interface générique ; les méthodes seront typées avec Produit.
     * On passe en deuxième argument int pour vérifier que Produit hérite bien de TableBase<int>
     *  (afin de valider la contrainte TableBase<TCle>)
     */
    internal class ProduitDao : IDao<Produit, int>
    {
        int IDao<Produit, int>.Inserer(Produit p) //Le paramètre générique TTable est remplacé par Produit
        {
            throw new NotImplementedException();
        }

        /* Implémentation explicite : la méthode RecupererTout n'est pas définie dans la classe ProduitDao.
         * Il faut donc obligatoirement passer par l'interface pour l'utilisation afin d'y accéder.
         */
        List<Produit> IDao<Produit, int>.RecupererTout()
        {
            throw new NotImplementedException();
        }

        /* Exemple en implémentation implicite :
         *  public List<Produit> RecupererTout()
         *  {
         *     throw new NotImplementedException();
         *  }
         */
    }
}
