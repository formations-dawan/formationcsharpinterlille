﻿namespace FormationCSharpInterLille.Genericite;

/* Paramètres génériques entre <> : le type sera connu à l'utilisation (passé en tant qu'argument du paramètre)
 * Ajouter une contrainte sur les types autorisés : where (param générique) : (contrainte)
 *  -> (contrainte) :
 *      - class : type référence
 *      - struct : type valeur
 *      - (interface) : le type doit implémenter (interface)
 *      - (classe) : le type doit hériter de (classe)
 *      - ...
 * Documentation : https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/where-generic-type-constraint
 */
internal interface IDao<TTable, TCle> where TTable : TableBase<TCle> // Le type TTable devra hériter de TableBase avec le type TCle correspondant
{
    List<TTable> RecupererTout();

    int Inserer(TTable p);
}
