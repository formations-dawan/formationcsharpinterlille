﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Genericite;

[DataContract]
internal abstract class TableBase<TCle>
{
    [DataMember]
    public TCle Id { get; set; }
}
