﻿using System.Runtime.Serialization.Json;
using System.Text;

namespace FormationCSharpInterLille.Genericite
{
    internal static class JsonUtils
    {
        public static string EnJson<T>(T obj)
        {
            // Création d'un nouveau sérialiseur JSON basé sur le type de T
            DataContractJsonSerializer ser = new(typeof(T)); //obj.GetType()

            using var stream = new MemoryStream(); // Création d'un nouveau flux mémoire (non managé)

            ser.WriteObject(stream, obj); // Serialise l'objet T en JSON et l'écrit dans le flux
            var res = Encoding.UTF8.GetString(stream.ToArray()); // Récupère le flux (octets brutes) pour le convertir en string

            return res;
            //stream.Dispose() : appelé automatiquement par using lors de la sortie de la méthode
        }
    }
}
