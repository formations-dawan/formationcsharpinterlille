﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Aggregation.Forte
{
    internal class Voiture
    {
        //Aggrégation forte : voiture ne peut exister sans moteur
        public Moteur Moteur { get; set; }

        // la voiture partage moteur avec d'autres objets (si voiture explose, comme par magie le moteur reste en vie)
        public Voiture(Moteur moteur)
        {
            Moteur = moteur;
        }

        //Composition : voiture gère la durée de vie du moteur (si voiture explose, le moteur aussi)
        public Voiture(string motorisation)
        {
            Moteur = new Moteur(motorisation);
        }

        public Voiture()
        {
        }

        //Association : voiture utilise Parking de manière temporaire
        public void Garer(Parking parc)
        {
            parc.Garer();
        }
    }
}
