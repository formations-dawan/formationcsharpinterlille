﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Aggregation.Forte
{
    internal class Moteur
    {
        public string Motorisation { get; }

        public Moteur(string motorisation)
        {
            Motorisation = motorisation;
        }
    }
}
