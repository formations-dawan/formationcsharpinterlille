﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormationCSharpInterLille.Aggregation.Faible
{
    internal class Client
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }

        // Aggrégation faible
        public Adresse Adresse { get; set; }
    }
}
